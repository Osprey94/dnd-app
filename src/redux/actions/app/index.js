export const SET_LOCALES_MESSAGES = 'SET_LOCALES_MESSAGES'
const setLocalesMessages = (data) => {
    return { type: SET_LOCALES_MESSAGES, payload: data }
}

export const SET_LOCALE = 'SET_LOCALE'
const setLocale = (data) => {
    return { type: SET_LOCALE, payload: data }
}

export default {
    setLocalesMessages,
    setLocale
}