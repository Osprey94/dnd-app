import messages from '../../../i18n'
import { SET_LOCALES_MESSAGES, SET_LOCALE } from '../../actions/app'

const initialState = {
    messagesPerLocale: messages,
    locales: Object.keys(messages),
    locale: 'es'
}
const messagesPerLocale = (state = initialState.messagesPerLocale, action) => {
    switch (action.type){
        case SET_LOCALES_MESSAGES:
            return action.payload
        default:
            return state
    }
}

const locale = (state = initialState.locale, action) => {
    switch(action.type){
        case SET_LOCALE:
            return action.payload
        default:
            return state
    }
}

const locales = (state = initialState.locales, action) => {
    switch(action.type){
        default:
            return state
    }
}

export default {
    name: 'app',
    reducers:  {
        messagesPerLocale,
        locale,
        locales
    }
}