import { combineReducers, createStore, applyMiddleware, compose} from 'redux'
import { promiseActionMiddleware } from 'fredux'

import appReducer from '../reducers/app'

const middlewares = applyMiddleware(promiseActionMiddleware)

const appReducersCombined = combineReducers({ ...appReducer.reducers })

const composeEnhancers = process.browser ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose

const rootReducer = combineReducers({
    [appReducer.name]: appReducersCombined
})

export const InitializeStore = () => {
    return createStore(rootReducer, composeEnhancers(middlewares))
}