import React, { useState } from 'react'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { InitializeStore} from '../redux/store'
import AppRouter from '../router'

const store = InitializeStore()

const App = () => {

  const [actualLocale, setActualLocale] = useState('es')
  const {
    app: {
      messagesPerLocale
    }
  } = store.getState()

  const changeLocale = (locale) => {
    setActualLocale(locale)
  }

  console.log('App reporting for service')
  return (
    <Provider store={store}>
      <IntlProvider
        key={actualLocale}
        messages={messagesPerLocale}
        locale={actualLocale}
      >
        <AppRouter changeLocale={changeLocale}/>
      </IntlProvider>
    </Provider>
  )
}

export default App
