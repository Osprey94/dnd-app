import React from 'react'
import { Router, navigate } from '@reach/router'
import { MainLayout } from '../components/layout'
import { Home, CharacterSheet } from '../views'

const AppRouter = ({changeLocale}) => {

    const goCharSheet = () => {
        navigate('/charactersheet')
    }
    const goHome = () => {
        navigate('/')
    }

    console.log('Router reporting for service')
    return(
        <React.Fragment>
            <Router>
                <MainLayout
                    path="/"
                    changeLocale={changeLocale}
                >
                    <Home
                        path="/"
                        goCharSheet={goCharSheet}
                    />
                    <CharacterSheet
                        path="charactersheet"
                        goHome={goHome}
                    />
                </MainLayout>
            </Router>
        </React.Fragment>
    )
}

export default AppRouter