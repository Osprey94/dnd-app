import React from 'react'


const Button = (props) =>{
    return (
        <button
            className={["el--button", props.classes, !props.children && "icon"].join(" ")}
            onClick={props.onClick}
            type={props.type || "button"}
            disabled={props.disabled}
        >
            {!props.showIcon && <span className={["text", props.center && "center"].join(" ")}>{props.children}</span>}
            {props.showIcon && props.children && <span className={["text", props.center && "center"].join(" ")}>{props.children}</span>}
            {props.showIcon && <span className={props.icon}/>}
            {props.tooltip && <span className="tooltip">{props.tooltip}</span>}
        </button>
    )
}

export default Button


/*
    Instructions:

*/