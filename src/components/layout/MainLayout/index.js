import React from 'react'
import './main.scss'
import { Header, Footer } from '../'

const MainLayout = ({changeLocale, children}) => {
    
    console.log('MainLayout reporting for service')
    return(
        <div>
            <Header 
            	changeLocale={changeLocale}
            />
            <main>{children}</main>
            <Footer />
        </div>
    )
}

export default MainLayout