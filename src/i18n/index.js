import en from './en/index.json'
import es from './es/index.json'

export default {
    en,
    es
}